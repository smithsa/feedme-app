from bs4 import BeautifulSoup
import requests
import sys
import os
class FeedMeCrawler:
	"""
		Class that gives web content from specified site_id given in the Parameters
		Parameters: Takes a site_id (string) and a number_of_articles (int)
		[site ids = articles:afterellen, articles:autostraddle, articles:hypetrak, tvshows:ch131]
		Note that a tvshows:ch131 does not need a defined number_of_articles
	"""
 	def __init__(self, site_id, number_of_articles = None):
 		self.site_name = []
 		self.web_content = []
	 
 		if(site_id == "articles:afterellen"):
 			self.web_content = self.get_articles_from_afterellen(number_of_articles)
 			self.site_name = "Afterellen"
 		elif(site_id == "articles:autostraddle"):
 			self.web_content = self.get_articles_from_autostraddle(number_of_articles)
 			self.site_name = "Autostraddle"
 		elif(site_id == "articles:blackgirldangerous"):
 			self.site_name = "Black Girl Dangerous"
 			self.web_content = self.get_articles_from_black_girl_dangerous(number_of_articles)
 		elif(site_id == "articles:hypetrak"):
 			self.site_name = "Hypetrak"
 			self.web_content = self.get_articles_from_hypetrak(number_of_articles)
 		elif(site_id == "tvshows:ch131"):
 			self.site_name = "Ch131"
	 		self.web_content = self.get_most_recent_episoides()

	def get_articles_from_autostraddle(self, number_of_articles):
		"""
			Function: Fetches article information from autostraddle
			Parameters: Takes an int of how many articles to fetsch
			Return: A list of dictionaries with the article information from the website
		"""
		request = requests.get('http://www.autostraddle.com/')
		html_string = request.content
		soup = BeautifulSoup(html_string)
		articles = soup.findAll("article", limit=number_of_articles)
		recent_articles = []
		for article in articles:
			title = article.find("header", {"class": "entry-header"}).find("h1").find("a").contents[0]
			link = article.find("header", {"class": "entry-header"}).find("h1").find("a")["href"]
			category = article.find("span", {"class":"cat-link"})
			if(category != None):
				category=category.find("a").contents[0]

			image_link = article.find("div", {"class":"featured-image"}).find("img")['src']
			# summary = article.find("div", {"class":"entry-summary"}).find("p", text=True).contents[0] 

			time_of_post = article.find("span", {"class":"posted-time"}).contents[0]
			recent_articles.append({"title":title, "link":link,"category":category ,"image_link":image_link, "time_of_post":time_of_post})

		return recent_articles

	def get_articles_from_afterellen(self, number_of_articles):
		"""
			Function: Fetches article information from afterellen
			Parameters: Takes an int of how many articles to fetsch
			Return: A list of dictionaries with the article information from the website
		"""
		request = requests.get('http://www.afterellen.com//')
		html_string = request.content
		soup = BeautifulSoup(html_string)
		articles = soup.findAll("article", limit=number_of_articles)
		recent_articles = []
		for article in articles:
			try:
				title = article.find("h4", {"class": "article-title"}).find("a")['title']
				link = article.find("h4", {"class": "article-title"}).find("a")["href"]
				image_link = article.find("figure").find("img")['src']
				time_of_post = article.find("time").contents[0]
				category = article.find("figure").find("div")
				if(category != None):
					category = category["class"][-1].split("_")[-1].capitalize()
				recent_articles.append({"title":title, "link":link, "image_link":image_link, "time_of_post":time_of_post, "category":category})
			except:
				continue

		return recent_articles

	def get_articles_from_black_girl_dangerous(self, number_of_articles):
		"""
			Function: Fetches article information from black girl dangerous
			Parameters: Takes an int of how many articles to fetsch
			Return: A list of dictionaries with the article information from the website
		"""
		request = requests.get('http://www.blackgirldangerous.org/')
		html_string = request.content
		soup = BeautifulSoup(html_string)
		articles = soup.findAll("div",{"class": "post"},limit=number_of_articles)
		recent_articles = []
		for article in articles:
			print article
			title = article.find("h2", {"class": "blog-post-title"}).find("a").contents[0]
			link = article.find("h2", {"class": "blog-post-title"}).find("a")["href"]
			image_link = article.find("img")['src']
			time_of_post = article.find("div", {"class":"blog-post-content"}).find("p").contents[0][:400]+" ..."
			recent_articles.append({"title":title, "link":link, "image_link":image_link, "time_of_post":time_of_post})

		return recent_articles

	def get_articles_from_hypetrak(self, number_of_articles):
		"""
			Function: Fetches article information from hypetrak
			Parameters: Takes an int of how many articles to fetsch 
			Return: A list of dictionaries with the article information from the website
		"""
		request = requests.get('http://hypetrak.com/')
		html_string = request.content
		soup = BeautifulSoup(html_string)
		articles = soup.findAll("div",{"class":"post clearfix"} ,limit=number_of_articles)
		recent_articles = []
		for article in articles:
			title = article.find("div", {"class": "title"}).find("a")['title']
			link = article.find("div", {"class": "title"}).find("a")["href"]
			image_link = article.find("div", {"class":"feature-image"}).find("img")['src']
			time_of_post = article.find("div", {"class":"info"}).find("div", {"class":"date"}).contents[0]
			category = article.find("div", {"class":"info"}).find("div", {"class":"category"}).contents[0]
			
			recent_articles.append({"title":title, "link":link, "image_link":image_link, "time_of_post":time_of_post, "category":category})
			
		return recent_articles

	def get_most_recent_episoides(self):
		"""
			Function: Fetches televisions shows from channel 131
			Parameters: None
			Return: A list of dictionaries with the show information
		""" 
		request = requests.get('http://www.ch131.so/Tvshows.html')
		html_string = request.content
		soup = BeautifulSoup(html_string)
		shows = soup.findAll("a")
		tv_shows = []
		site_name = "http://www.ch131.so/" 
		wanted_shows = ['Recreation ', 'Shameless', 'Modern Family', 'Pretty Little Liars', 'Revolution', 'The Fosters',
							'The Good Wife', 'Scandal', "Grey", 'Parenthood', 'The Mindy Project']
		
		#getting the tv shows that are desired
		links_of_wanted_shows = []	
		seasons_links_list = []
		for show in shows:	
			for wanted_show in wanted_shows:
				if(wanted_show in show.contents[0]):
					links_of_wanted_shows.append((show.contents[0], site_name+show['href']))
		
		#getting the most recent seasons and associated information
		most_recent_season = []
		for wanted_show_link in links_of_wanted_shows:
			request = requests.get(wanted_show_link[1])
			html_string = request.content
			new_soup = BeautifulSoup(html_string)
			season_link_html = new_soup.findAll("a")[-1]
			season = season_link_html.contents[0]
			season_link = site_name + season_link_html["href"]
			most_recent_season.append((wanted_show_link[0], season_link, season))


		#getting the most recent episoides and associated information
		most_recent_episoides = []
		for season in most_recent_season:
			request = requests.get(season[1])
			html_string = request.content
			new_soup = BeautifulSoup(html_string)
			episoide_link_html = new_soup.findAll("a")[-1]
			episoide_title = episoide_link_html.contents[0]
			episoide_link = site_name + episoide_link_html["href"]
			#delete after the fosters are out of season 1
			if(season[0] == "The Fosters"):
				episoide_link = season[1]

			most_recent_episoides.append((season[0], episoide_link, episoide_title, season[2]))

		#getting the iframes
		id_counter = 0
		for episoide in most_recent_episoides:
			request = requests.get(episoide[1])
			html_string = request.content
			new_soup = BeautifulSoup(html_string)
			iframe = new_soup.find("iframe")
			if(iframe != None):
				iframe = iframe["src"]
			id_counter += 1
			show_id = str(id_counter)
			tv_shows.append({"show":episoide[0],"iframe":iframe, "season":episoide[3], "episoide":episoide[2], "id":show_id})

		return tv_shows


def main():
	print get_articles_from_black_girl_dangerous(19)

if __name__ == '__main__':
	main()