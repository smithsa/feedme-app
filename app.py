from flask import Flask
from flask import render_template
from flask import request
from flask import url_for
from flask import session, flash, redirect
from flask import jsonify
from feedme_crawler import FeedMeCrawler
from private_constants import *
import os

template_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'templates')
app = Flask(__name__, template_folder=template_dir)\

@app.route('/')
@app.route('/login', methods=['GET', 'POST'])
def login():
	"""The login page for the site"""
	error = None
	session['logged_in'] = False
	if request.method == 'POST':
		username = request.form['username']
		password = request.form['password']
		if username not in LOGIN_CREDENTIALS.keys():
			error = 'Invalid username'
		elif password != LOGIN_CREDENTIALS[username]:
			error = 'Invalid password'
		else:
			session['logged_in'] = True
			flash('You were logged in')
			return redirect(url_for('articles'))

	if( session['logged_in']):
		return redirect(url_for('articles'))
		
	return render_template('login.html', error=error)

@app.route('/logout')
def logout():
	"""The logout action for the page, redirects to the login page"""
	session.pop('logged_in', None)
	flash('You were logged out')
	return redirect(url_for('login'))


@app.route('/articles')
def articles():
	"""The articles page that dispalys all articles"""
	if(session['logged_in'] != True):
		return redirect(url_for('login'))
	number_of_articles = 5
	articles = []
	articles_input = []
	afterellen = FeedMeCrawler("articles:afterellen", number_of_articles)
	articles.append(afterellen)
	articles_input.append({"id":"afterellen", "name":"Afterellen", "is_selected":True})
	autostraddle = FeedMeCrawler("articles:autostraddle", number_of_articles)
	articles.append(autostraddle)
	articles_input.append({"id":"autostraddle", "name":"Autostraddle", "is_selected":True})
	hypetrak = FeedMeCrawler("articles:hypetrak", number_of_articles)
	articles.append(hypetrak)
	articles_input.append({"id":"hypetrak", "name":"Hypetrak", "is_selected":True})
	blackgirldangerous = FeedMeCrawler("articles:blackgirldangerous", number_of_articles)
	articles.append(blackgirldangerous)
	articles_input.append({"id":"blackgirldangerous", "name":"Black Girl Dangerous", "is_selected":True})
	return render_template('index.html', articles=articles, articles_input=articles_input, display_setting=True, current_num_articles=number_of_articles)

@app.route('/filter_articles', methods=['POST'])
def filter_articles():
	"""The filter articles action, that renders the update in changes to the articles displayed on the site"""
	if(session['logged_in'] != True):
		return redirect(url_for('login'))
	articles = []
	articles_input = [] 
	number_of_articles = int(request.form["number_of_articles"])
	form_items = request.form.getlist("filters[]")

	#sifting through data to see what to return to render in the view
	if("afterellen" in form_items):
		afterellen = FeedMeCrawler("articles:afterellen", number_of_articles)
		articles.append(afterellen)
		articles_input.append({"id":"afterellen", "name":"Afterellen", "is_selected":True})
	else:
		articles_input.append({"id":"afterellen", "name":"Afterellen", "is_selected":False})

	if("autostraddle" in form_items):
		autostraddle = FeedMeCrawler("articles:autostraddle", number_of_articles)
		articles.append(autostraddle)
		articles_input.append({"id":"autostraddle", "name":"Autostraddle", "is_selected":True})
	else:
		articles_input.append({"id":"autostraddle", "name":"Autostraddle", "is_selected":False})

	if("blackgirldangerous" in form_items):
		autostraddle = FeedMeCrawler("articles:blackgirldangerous", number_of_articles)
		articles.append(autostraddle)
		articles_input.append({"id":"blackgirldangerous", "name":"Black Girl Dangerous", "is_selected":True})
	else:
		articles_input.append({"id":"blackgirldangerous", "name":"Black Girl Dangerous", "is_selected":False})

	if("hypetrak" in form_items):
		hypetrak = FeedMeCrawler("articles:hypetrak", number_of_articles)
		articles.append(hypetrak)
		articles_input.append({"id":"hypetrak", "name":"Hypetrak", "is_selected":True})
	else:
		articles_input.append({"id":"hypetrak", "name":"Hypetrak", "is_selected":False})
	

	return render_template('index.html', articles=articles, articles_input=articles_input, display_setting=True, current_num_articles=number_of_articles)


@app.route('/tvshows')
def tvshows():
	"""The TV show page that displays videos of the tv shows pulled"""
	if(session['logged_in'] != True):
		return redirect(url_for('login'))
	ch131 = FeedMeCrawler("tvshows:ch131")
	ch131_content = [ch131.site_name, ch131.web_content]
	return render_template('tvshows.html', ch131=ch131_content)

@app.errorhandler(404)
def page_not_found(e):
	"""The 404 Page, that will display when a page is not found"""
	return render_template('404.html')
	
if __name__ == '__main__':
	app.secret_key = SECRET_KEY
	app.debug = False
	port = int(os.environ.get('PORT', 5000))
	app.run(host='0.0.0.0', port=port)